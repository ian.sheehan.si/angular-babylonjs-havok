
module.exports = {

  module: {
    rules: [
      {
        test: /\.wasm$/,
        loader: 'wasm-loader',
      },
    ],
  },

  // Your custom webpack configuration goes here
  // original only below
  // AMMO PHYSICS WORKS BECAUSE THIS SETTINGS
  node: {
    fs: 'empty',
    path: 'empty'
  }

};

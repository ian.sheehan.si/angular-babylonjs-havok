import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {Demo2Component} from "./demo2/demo2.component";
import {MenuComponent} from "./menu/menu.component";


const routes: Routes = [
  {
    path: '',
    component: Demo2Component
  },

  {
    path: 'demo2',
    component: Demo2Component
  },
  {
    path: 'menu',
    component: MenuComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

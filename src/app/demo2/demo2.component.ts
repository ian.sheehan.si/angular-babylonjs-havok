/*
  Try HavokPhysics - not working with angular (webpack) and "@babylonjs/havok": "^1.1.4",
  https://www.npmjs.com/package/@babylonjs/havok
  https://webpack.js.org/configuration/resolve

  Example of initialization Havok physics
  Look At: https://doc.babylonjs.com/features/featuresDeepDive/physics/havokPlugin
  URL:
 */


import {Component, ElementRef, ViewChild} from '@angular/core';
import * as BABYLON from "@babylonjs/core";
import Ammo from "ammojs-typed";
import HavokPhysics from "@babylonjs/havok";

import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";

import {GridMaterial} from "@babylonjs/materials";
import {PhysicsEngine, HavokPlugin} from "@babylonjs/core/Physics";


@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component {
  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas!: ElementRef<HTMLCanvasElement>;

  private canvas!: HTMLCanvasElement;
  private engine!: BABYLON.Engine;
  public scene!: BABYLON.Scene;

  public constructor() {
    console.log('BEGIN - constructor()');

    console.log('END - constructor()');
  }

  public async ngOnInit() {
    console.log('BEGIN - ngOnInit()');

    await this.createScene(this.rendererCanvas);

    console.log('END - ngOnInit()');
  }

  public async createScene(rendererCanvas: ElementRef<HTMLCanvasElement>) {
    console.log('BEGIN - createScene()');
    this.canvas = rendererCanvas.nativeElement;
    this.engine = new BABYLON.Engine(this.canvas, true);
    this.scene = new BABYLON.Scene(this.engine);

    //////////
    // INIT / SETUP -> AMMO PHYSICS
    /*
    const ammoAPI = await Ammo.bind(window)();
    const gravityVector: BABYLON.Vector3 = new BABYLON.Vector3(0, -9.81, 0);
    const ammoPlugin: BABYLON.AmmoJSPlugin = new BABYLON.AmmoJSPlugin(true, ammoAPI);
    this.scene.enablePhysics( gravityVector,ammoPlugin);
    */
    //////////


    //////////
    // INIT / SETUP -> HAVOK PHYSICS

    const havokAPI = await HavokPhysics({
      locateFile: (file) => {
        // return "assets/HavokPhysics.wasm"
        // return "file:///C://Users/Bogdan/Projects/WebstormProjects/angular-with-babylonjs-with-ammo-physics/node_modules/@babylonjs/havok/lib/esm"
        // return file;
        console.log(`https://preview.babylonjs.com/havok/${file}`)
        return `https://preview.babylonjs.com/havok/${file}`;
      }
    });
    const gravityVector: BABYLON.Vector3 = new BABYLON.Vector3(0, -9.81, 0);
    const havokPlugin: BABYLON.HavokPlugin = new BABYLON.HavokPlugin(true, havokAPI);
    this.scene.enablePhysics( gravityVector, havokPlugin );

    //////////


    // This creates and positions a free camera (non-mesh)
    // let camera = new FreeCamera("camera1", new Vector3(0, 5, -10), this.scene);
    let camera = new BABYLON.ArcRotateCamera(
      "ArcRotateCamera",
      -Math.PI / 4, (Math.PI / 3),
      20,
      BABYLON.Vector3.Zero(),
      this.scene );

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(this.canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), this.scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Create a grid material
    let material = new GridMaterial("grid", this.scene);

    // Our built-in 'sphere' shape.
    let sphere = BABYLON.CreateSphere("Ball", {segments: 16, diameter: 2}, this.scene);
    sphere.position.x = 0;
    sphere.position.y = 10;

    const sphereBody = new BABYLON.PhysicsBody(sphere, BABYLON.PhysicsMotionType.DYNAMIC, false, this.scene);
    sphereBody.setMassProperties( { mass: 0.5} );
    const sphereShape = new BABYLON.PhysicsShapeSphere( new BABYLON.Vector3(0,0,0), 1, this.scene)
    sphereBody.shape = sphereShape;
    const sphereMaterial = { friction: 0.2, restitution: 0.3 };
    sphereShape.material = sphereMaterial;

    const polyhedron = BABYLON.MeshBuilder.CreatePolyhedron("oct", {type: 1, size: 1}, this.scene);
    polyhedron.position.y = 20;
    polyhedron.position.x = 2;
    const polyhedronBody = new BABYLON.PhysicsBody(polyhedron, BABYLON.PhysicsMotionType.DYNAMIC, false, this.scene);
    polyhedronBody.setMassProperties( { mass: 0.5} );
    const polyhedronShape = new BABYLON.PhysicsShapeConvexHull( polyhedron, this.scene)
    polyhedronBody.shape = polyhedronShape;
    const polyhedronMaterial = { friction: 1.5, restitution: 0.9 };
    polyhedronShape.material = polyhedronMaterial;

    // Affect a material
    //sphere.material = material;

    // Our built-in 'ground' shape.
    const ground = BABYLON.CreateGround("ground1", {width: 64, height: 64, subdivisions: 2}, this.scene);
    const groundBody = new BABYLON.PhysicsBody(ground, BABYLON.PhysicsMotionType.STATIC, false, this.scene);
    const groundShape = new BABYLON.PhysicsShapeBox(
      new BABYLON.Vector3(0, 0, 0),
      BABYLON.Quaternion.Identity(),
      new BABYLON.Vector3(64, 0.1, 64),
      this.scene
    );
    const groundMaterial = { friction: 0.2, restitution: 0.3 };
    groundShape.material = groundMaterial;
    groundBody.shape = groundShape;
    groundBody.setMassProperties({
      mass: 0,
    });

    // Affect a material
    ground.material = material;

    // @ts-ignore
    console.log("Scene is using Physics: "+ this.scene.getPhysicsEngine().getPhysicsPluginName());
    await this.scene.debugLayer.show();
    // await this.scene.debugLayer.show();
    // Render every frame
    this.engine.runRenderLoop(() => {
      this.scene.render();
    });
    console.log('END - createScene()');
  }
}
